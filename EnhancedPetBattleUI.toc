## Interface: 50400
## Title: |cff1784d1EnhancedPetBattleUI|r
## Author: Azilroka, Sortokk
## Version: 2.04
## Notes: A Pet Battle Heads Up Display Frame
## OptionalDeps: Tukui, ElvUI, AsphyxiaUI-Core, DuffedUI, Enhanced_Config
## SavedVariables: EnhancedPetBattleUIOptions
## X-Tukui-ProjectID: 139
## X-Tukui-ProjectFolders: EnhancedPetBattleUI

EnhancedPetBattleUI.lua

